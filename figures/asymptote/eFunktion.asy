import graph;
size(200,0);
real f(real x) {return exp(x);}
pair F(real x) {return (x,f(x));}
// operator .. calls Bezier cubic spline interpolation
draw(graph(f,-4,2,operator ..));
xaxis("$x$",
      Ticks(Label(align=E),
	    NoZero,
          Step=1, 
          Size=1mm, 
	    pTick=black));
yaxis("$y$",
      Ticks(Label(align=E),
	    NoZero,
	    begin=false,beginlabel=false,
          Step=1, 
          Size=1mm, 
	    pTick=black));
label("$e^x$",F(1),SE);
