// Tu Problem 4.1
// 2018-06-23 pv die zwei Niveau-Flächen (Quadrate) nun so, dass die Normale durch die Mittelpunkte geht.

import graph3;

size(400,0);

currentprojection=perspective(-26,-50,5.5);

real ip; // value of inner product

xaxis3(Label("$x$"),-15,15,axis=YZZero(extend=false),grey,Arrow3);
yaxis3(Label("$y$"),-15,15,axis=XZZero(extend=false),grey,Arrow3);
zaxis3(Label("$z$"),-5,15,axis=XYZero(extend=false),grey,Arrow3);

// für z=1: Rahmen und zugehöriges Fadenkreuz durch den Ursprung
draw((-10,0,1)--(10,0,1),grey);
draw((0,-10,1)--(0,10,1),grey);
// für z=10: Rahmen und zugehöriges Fadenkreuz durch den Ursprung
draw((-10,0,10)--(10,0,10),grey);
draw((0,-10,10)--(0,10,10),grey);

//----- (*) vectorfield

for (int i=-1; i<=1; ++i) {
  for (int j=-1; j<=1; ++j) {
    real x = 10i;
    real y = 10j;
    real z = 1;
    real vx = 2j;
    real vy = 2i;
    real vz = 0;
    real ip;
    string ipstring;
    draw((x,y,z)--(x+vx,y+vy,z+vz),red,Arrow3);

    z = 10;
    draw((x,y,z)--(x+vx,y+vy,z+vz),red,Arrow3);
  }
}






