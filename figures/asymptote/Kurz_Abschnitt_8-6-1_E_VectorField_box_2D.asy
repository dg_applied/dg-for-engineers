//
// Show E, the electrical field (vector field) for the exercise in this section of Lehner&Kurz
//
// V 1.0 2024-06-29 ok for script
// V 1.1 2024-07-01 used bool condition(pair) to exclude vectors inside the pillbox
//
import graph;
defaultpen(1.0);
// unitsize(1cm); // make user coordinates represent multiples of exactly 1 cm
size(16cm,0); // picture shall be 8cm in width, height shall be natural height for this scaling factor
//
// geometry and physics of the exercise
include "./Kurz_Abschnitt_8-6-1_Setting.asy";
//
// vectorfield (see Hammerlindl, 2020, p. 125
//
path vector(pair v) {
  real factor_1   = -Q/(4*pi*epsilon0);
  real factor_2_z = ( -((v.x+a) / (((v.x+a)^2+v.y^2)^(3/2)))
	              +((v.x-a) / (((v.x-a)^2+v.y^2)^(3/2))) );
  real factor_2_r = ( -  v.y    / (((v.x+a)^2+v.y^2)^(3/2))
                      +  v.y    / (((v.x-a)^2+v.y^2)^(3/2))  );
  real x, y;
  x = factor_1 * factor_2_z;
  y = factor_1 * factor_2_r;
  // scale the vector so that it is reduced to the log of its original length
  // this should diminish big discrepancies in vectorlength
  real vectorlength = sqrt(x^2 + y^2);
  real log_vectorlength = log(vectorlength);
  real scale = log_vectorlength / vectorlength;
  // write (v.x, v.y);
  // write (x,y);
  // write (scale);
  // write (x*scale, y*scale);
  return ( 0 , 0 )--( x*scale , y*scale );
  // return ( 0 , 0 )--( x , y );
}

// part of the box where not to draw vectors.
bool condition(pair v) {
  if (v.x >=0 && v.y < pillbox_radius) return false;
  else return true;
}

// path vector1(pair v) {return (0,0)--(sin(v.x),cos(v.y));} for testing only

// Pay attention not to include location of Q in the grid of vectorfield
// 1. It is better to begin the vectorfield display far away from Q:
//   then, the range of vector lengths is smaller and one can better recognize differences
// 2. Or you use logarithm on the vector length and then you might display the vectorfield
//   in the whole domain minus the location of the charge.
//   In this case there will be a separate scaling for each call of vectorfield()
// test:
// add(vectorfield(vector,(-4,-4),(0,4),3,3,truesize=false));
//
// for the script:
int nmeshx=5; // n of meshes
int nmeshy=4; // n of meshes
// add(vectorfield(vector,(-(nmeshx*meshlength),pillbox_stub_r),(0,pillbox_radius+meshlength),nmeshx,nmeshy, truesize=false));
//
// test: use the current source code of asymptote (as of 2024-06-30)
// show a vectorfield in a box
include "./module/vectorfield_2D_asymptote.asy";
int nmeshx=6; // n of points at which to draw a vector
int nmeshy=5; // n of points at which to draw a vector
pair location;
add(vectorfield_2D_asymptote(vector,(-(nmeshx-1)*meshlength,pillbox_stub_r),(0,pillbox_radius+meshlength),nmeshx,nmeshy,truesize=false,cond=condition));
