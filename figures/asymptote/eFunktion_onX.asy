import graph;
size(200,0);
real xmin=-4;
real xmax=2;
real xplus=0.1; // indicate that we have an open interval
real f(real x) {return exp(x);}
pair F(real x) {return (x,f(x));}
// pair Finv(real x) {return (log(x),0);}

path bar (real x, bool domain) {
  if (domain == true) // we want the tick to point towards tha domain value
    return((x,0.0) -- (x,0.1));
  else
    return((x,0.0) -- (x,-0.1));
}

string label;

xaxis(xmin-xplus, xmax+xplus);

// codomain

real r=0.5;
int labelnumber=0;
while (log(r)<=xmax) {
  draw(bar(log(r), false), deepgreen);
  label(rotate(90)*format("$%.2f$", r), (log(r),-0.8-1*(labelnumber%3)),deepgreen);
  r=r+0.5;
  ++labelnumber;
}

// domain

// x-Achse als zweites gezeichnet weil die schwarzen ticks "siegen" sollen

for (int i=(int) xmin; i<=(int) xmax; ++i) {
  draw(bar(i, true), black);
  label(format("$%d$", i), (i,0.5),black);
}




