// this takes a copy of vectorfield() from the repository at sourceforge on 2024-06-30
// and changes it in order to draw a 1-form
//
// return a 1-form over box(a,b).

// covector_icon() draws an icon as it is shown in misner, thorpe, wheeler: Gravitation
//
// --input--
// p        - point of the manifold where to draw covector_icon
// covector - endpoint of covector assuming startpoint is in origin
// bl       - bar length, length of little bar at start and end point
// comp     - graphical component of the icon (out of {0,1,2})
// --output--
// path[]   - array of paths representing a covector icon
// path[] covector_icon(pair p, pair covector, real bl) {
path covector_icon(pair p, pair covector, real bl, int comp) {
  // written with the help of "GM" on https://asy.marris.fr/forum/ on 2021-05-16
  path bar = (0,bl/2)--(0,-bl/2),                           // little bar at start and end point
    tip = (bl/2,bl/2)--(0,0)--(bl/2,-bl/2);                 // tip of arrow
  // write(bar,tip);
  pair A=p, B=A+covector;                                   // start and end point
  // depending on comp return all, start bar, end bar or tip of the covector icon
  if (comp == 0)
    return (shift(p)*A--B);       // straight line start--end  
  if (comp == 1)
    return (shift(p)*rotate(degrees(covector))*bar);        // start bar
  if (comp == 2)
    return (shift(p+covector)*rotate(degrees(covector))*bar); // end bar
  if (comp == 3)
    return (shift(p+covector)*rotate(degrees(covector)-180)*tip); // tip
  return(nullpath);
}

// OneForm_2D_like_asymptote() is a copy from the original vectorfield() function of asymptote
// where we change things in order to draw a 1-form in 2D
//
// --input--
// path covector(pair) - function reads a position and returns a vector icon
// a,b                 - defines box with a=lower left corner and b=upper right corner
// nx,ny               - number of points at which to draw a vector
// truesize            - ?
// bool cond(pair)     - a condition restricting at which location to draw a vector
// p                   - pen
// arrow               - arrow
// margin              - ?
// --output--
// picture             - the box filled with vectors

picture OneForm_2D_like_asymptote(path covector(pair), pair a, pair b, int nx=nmesh, int ny=nx, bool truesize=false, bool cond(pair z)=null, pen p=currentpen, arrowbar arrow=Arrow, margin margin=PenMargin) 
{ 
  picture pic;
  real dx=(b.x-a.x)/(nx-1);
  real dy=(b.y-a.y)/(ny-1); 
  bool all=cond == null; 
  real scale;
  transform t=scale(dx,dy); // scale by dx in x-direction and by dy in y-direction

  write("OneForm_2D_like_asymptote, dx,dy:", dx, dy);

  // pair size(pair z) - tell me size of vector at location z after scaling it to the mesh 
  // --input--
  // pair z - a location where to place a vector
  // --output--
  // pair   - horizontal and vertical length of user's vector
  //          after scaling it according to mesh rectangle
  pair size(pair z) {
    // scale path provided by caller function covector() horizontally and vertically
    // according to mesh rectangle 
    path g=t*covector(z); 
    int n=size(g); // number of nodes in path g
    // ?
    pair w=n == 1 ? point(g,0) : point(g,n-1)-point(g,0); 
    return (abs(w.x),abs(w.y)); 
  } 

  // A. provide max. max is the maximum horizontal and vertical length over all user vectors
  // 
  // max horizontal and vertical length of user's vector in lower left corner
  // max will be an argument for maxbound() in the initial iteration of the two nested loops below
  pair max=size(a);

  write("OneForm_2D_like_asymptote, max:", max);

  for(int i=0; i < nx; ++i) { 
    real x=a.x+i*dx; 
    for(int j=0; j < ny; ++j) { 
      real y=a.y+j*dy; max=maxbound(max,size((x,y))); 
    } 
  }

  // B. provide scale. scale=1.0                    (max.x==0 && max.y==0)
  //                   scale=dx/max.x               (max.x!=0 && max.y==0)
  //                   scale=min(dx/max.x,dy/max.y) (max.x!=0 && max.y!=0)  
  if(max.x == 0) scale=max.y == 0 ? 1.0 : dy/max.y; 
  else if(max.y == 0) scale=dx/max.x; 
  else scale=min(dx/max.x,dy/max.y);

  write("OneForm_2D_like_asymptote, max.x, max.y:", max.x, max.y);
  //
  write("OneForm_2D_like_asymptote, scale:", scale);
  write("OneForm_2D_like_asymptote, dy/max.y:", dy/max.y);
  write("OneForm_2D_like_asymptote, dx/max.x:", dx/max.x);
  write("OneForm_2D_like_asymptote, min(dx/max.x,dy/max.y):", min(dx/max.x,dy/max.y));

  // C. create mesh
  for(int i=0; i < nx; ++i) { 
    real x=a.x+i*dx; 
    for(int j=0; j < ny; ++j) { 
      real y=a.y+j*dy; 
      pair z=(x,y); // location where to place a single vector
      write("OneForm_2D_like_asymptote, z:", z);
      // draw if
      // all     - no condition depending on the location was stated OR
      // cond(z) - a condition depending on the location was stated and is met 
      if(all || cond(z)) {
	// covector(z)  - call function "covector()" provided by the caller
	// t            - scale by (dx,dy)
	// scale(scale) - scale by scale (depending on maximum size of user vector)
	// size(v)      - number of nodes of path v
	path v=scale(scale)*t*covector(z);
	// if there is 1 node only in path, return (0,0)--v, otherwise v.
	write("OneForm_2D_like_asymptote, size(v):", size(v));
        for (int vi=0;i<size(v);++i) {
	    write("OneForm_2D_like_asymptote, point(v,vi):", point(v,vi));
	  }
	path g=size(v) == 1 ? (0,0)--v : v;
	// write(v,g);
	// draw at location z            - (truesize==true)
	// draw g after shifting it to z - (truesize==false)
	//
	// z     - location where to place a single vector
	// pic   - current picture
	// g     - some modification of v (which I do not understand yet)
	// p     - current pen
	// arrow - 
	if(truesize) draw(z,pic,g,p,arrow);
	// Hammerlindl, 2020, p. 15
	// "Margins can be used to shrink the visible portion of a path by labelmargin(p)
        // to avoid overlap with other drawn objects."
	else draw(pic,shift(z)*g,p,arrow,margin); 
      } 
    } 
  } 

  return pic; 
} 
