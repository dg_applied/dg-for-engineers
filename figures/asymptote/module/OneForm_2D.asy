// 2D-diagram for one-form 
// tries to emulate vectorfield() from asymptote

//import graph;
//size(400);

real meshx, meshy;

// p        - point of the manifold where to draw covector_icon
// covector - endpoint of covector assuming startpoint is in origin
// bl       - bar length, length of little bar at start and end point
path[] covector_icon(pair p, pair covector, real bl) {
  // written with the help of "GM" on https://asy.marris.fr/forum/ on 2021-05-16
  path bar = (0,bl/2)--(0,-bl/2),                   // little bar at start and end point
    tip = (bl/2,bl/2)--(0,0)--(bl/2,-bl/2);         // tip of arrow
  // veraltet: transform t1 = scale(1/(length(p))^2),
  // veraltet: transform t2 = shift(p)*rotate(-90);
  pair A=p, B=A+covector;                           // start and end point
  // Je retourne un array de 3 path formant la flèche.
  return new path[] {shift(p)*rotate(degrees(covector))*bar, // bar 1 at startpoint
      shift(p+covector)*rotate(degrees(covector))*bar,     // bar 2 at endpoint
      shift(p+covector)*rotate(degrees(covector)-180)*tip      // tip 2 at endpoint
      };
}

// OneForm() tries to emulate vectorfield() - see Hammerlindl, 2020, p. 125, item 14.
// 
// covector - provided by user as a path depending on a pair
// a        - lower left corner of box
// b        - upper right corner of box
// nx       - number of meshes in x-direction
// ny       - number of meshes in y-direction
//
typedef picture OneForm_2D(pair covector(pair), pair a, pair b, int nx=nmesh, int ny=nx); 
picture OneForm_2D(pair covector(pair), pair a, pair b, int nx, int ny)
{
  meshx = (b.x-a.x)/nx;
  meshy = (b.y-a.y)/ny;
  picture pic=new picture;
  for(int ix=0; ix<=nx; ++ix) {
    for (int iy=0; iy<=ny; ++iy) {
      // pt - where to draw a single covector_icon
      pair pt=(a.x+ix*meshx, a.y+iy*meshy);
      // >>>klären, warum das einen Fehler liefert: dot(pt,3bp);
      draw(covector_icon(pt, covector(pt), min(meshx, meshy)*0.15));
    } 
  }
  return pic;
}
 
