// this is a copy from the repository at sourceforge on 2024-06-30
// the only change are
// - that we name this function vectorfield_2D_asymptote()
// - that we add comments
//
// return a vector field over box(a,b).

// --input--
// path vector(pair)   - function reads a position and returns a vector icon
// a,b                 - defines box with a=lower left corner and b=upper right corner
// nx,ny               - number of points at which to draw a vector
// truesize            - ?
// bool cond(pair)     - a condition restricting at which location to draw a vector
// p                   - pen
// arrow               - arrow
// margin              - ?
// --output--
// picture             - the box filled with vectors

picture vectorfield_2D_asymptote(path vector(pair), pair a, pair b, int nx=nmesh, int ny=nx, bool truesize=false, bool cond(pair z)=null, pen p=currentpen, arrowbar arrow=Arrow, margin margin=PenMargin) 
{ 
  picture pic;
  real dx=(b.x-a.x)/(nx-1);
  real dy=(b.y-a.y)/(ny-1); 
  bool all=cond == null; 
  real scale;
  transform t=scale(dx,dy); // scale by dx in x-direction and by dy in y-direction

  write("vectorfield_2D_asymptote, dx,dy:", dx, dy);  

  // --input--
  // pair z - a location where to place a vector
  // --output--
  // pair   - horizontal and vertical length of user's vector
  //          after scaling it according to mesh rectangle
  pair size(pair z) {
    // scale path provided by caller function vector() horizontally and vertically
    // according to mesh rectangle 
    path g=t*vector(z); 
    int n=size(g); // number of nodes in path g
    // ?
    pair w=n == 1 ? point(g,0) : point(g,n-1)-point(g,0); 
    return (abs(w.x),abs(w.y)); 
  } 

  // A. provide max. max is the maximum horizontal and vertical length over all user vectors
  // 
  // max horizontal and vertical length of user's vector in lower left corner
  // max will be an argument for maxbound() in the initial iteration of the two nested loops below
  pair max=size(a); 

  for(int i=0; i < nx; ++i) { 
    real x=a.x+i*dx; 
    for(int j=0; j < ny; ++j) { 
      real y=a.y+j*dy; max=maxbound(max,size((x,y))); 
    } 
  }

  write("vectorfield_2D_asymptote, max:", max);
  
  // B. provide scale. scale=1.0                    (max.x==0 && max.y==0)
  //                   scale=dx/max.x               (max.x!=0 && max.y==0)
  //                   scale=min(dx/max.x,dy/max.y) (max.x!=0 && max.y!=0)  
  if(max.x == 0) scale=max.y == 0 ? 1.0 : dy/max.y; 
  else if(max.y == 0) scale=dx/max.x; 
  else scale=min(dx/max.x,dy/max.y);

  //
  write("vectorfield_2D_asymptote, scale:", scale);

  // C. create mesh
  for(int i=0; i < nx; ++i) { 
    real x=a.x+i*dx; 
    for(int j=0; j < ny; ++j) { 
      real y=a.y+j*dy; 
      pair z=(x,y); // location where to place a single vector
      write("vectorfield_2D_asymptote, z:", z);
      // draw if
      // all     - no condition depending on the location was stated OR
      // cond(z) - a condition depending on the location was stated and is met 
      if(all || cond(z)) {
	// vector(z)    - call function vector() provided by the caller
	// t            - scale by (dx,dy)
	// scale(scale) - scale by scale (depending on maximum size of user vector)
	path v=scale(scale)*t*vector(z);
	// ?
	path g=size(v) == 1 ? (0,0)--v : v;
	// draw at location z            - (truesize==true)
	// draw g after shifting it to z - (truesize==false)
	if(truesize) draw(z,pic,g,p,arrow);
	// Hammerlindl, 2020, p. 15
	// "Margins can be used to shrink the visible portion of a path by labelmargin(p)
        // to avoid overlap with other drawn objects."
	else draw(pic,shift(z)*g,p,arrow,margin); 
      } 
    } 
  } 

  return pic; 
} 
