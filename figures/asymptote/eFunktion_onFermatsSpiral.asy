import graph;
size(200,0);
//
real xmin=-4.0;
real xmax=2.0;
real aunsigned=1;
//
path spiral;
pair p;
// number of bezier nodes to get a sufficiently smooth spiral needs to be determined by experiment.
// After some experiments we chose:
real incr=0.01;
int incrModulus=100;
// Note: we will not put tickmarks on all these nodes, we will only select the values
// -4, -3, -2, -1, 0, 1, 2.
// This means that the programmer needs to observe this relationship between incr and incrModulus: incr * (real) incrModulus = 1.0
//
// The nodes in the Bezier-curve are created through a 2-dimensional array (unsorted nodes in T, sorted nodes in U).
// Chose a 2-dimensional arrays since I found a function sort(Array) in Hammerlindl, acting on 2-dimensional arrays, sorting them by the first column.
real[][] T=new real[][]; // list of pairs of reals. 1st real: parameter for Fermat spiral, 2nd real: 0 for domain mark, 1.0 for codomain mark
real[][] U=new real[][]; // U will be T after a sort according to its first column
//
// Description. A function (like the exponential function) can be shown in a x,y-diagram with straight axes.
// But it can also be shown in one dimension: showing domain and codomain values on one straight axis.
// And finally it can be shown in one dimension but on a curvilinear axis. This is done with this
// program. We use Fermat's spiral as the curvilinear axis.
// 
// We create the spiral/curve out of a list of points (being "nodes" of a "path").
// In order to do this we create a list of equidistant domain values, then a list of equidistant
// codomain values, then we merge these two lists and sort the entries.
// At the end we create a path from that.

// Determine (x,y)-pair of Fermat spiral for a parameter d
pair FermatsSpiral(real d) {
  if (d<0)
    // different branches for negative and positive x
    return(-aunsigned*sqrt(-d)*cos(-d),-aunsigned*sqrt(-d)*sin(-d));  
  else
    return(aunsigned*sqrt(d)*cos(d),aunsigned*sqrt(d)*sin(d));
}

// Some interval on the x-axis, a neighborhood of the origin, gets mapped on Fermat's spiral.
// The point where the two branches of the spiral join shall correspond to the origin.
// The nodes of the Bezier-curve will be created from array T bezogen which is created from:
// - monotonically increasing values of the domain
// - monotonically increasing values of the codomain
path FermatsSpiralAxis (path spiral) {

  // domain: some values

  real r=xmin;
  while (r<=(xmax+incr/2)) { // adding incr/2 necessary in order to get biggest r at xmax. Weird behavior or something I do not understand yet.
    real[] arr;
    arr[0]=r;
    arr[1]=-1.0; // -1.0 is a code for: this is a domain value
    T.push(arr); 
    // label(format("$%d$", i),labelLocationOnFermatsSpiral(i,true),black);
    r=r+incr;
  }

  // codomain: some values

  real r=0.5; // starting value of exponential function we consider as a level set
  while (log(r)<=xmax) { // is the corresponding x-value greater than what we examine here?
    real[] arr;
    arr[0]=log(r); // x-value that corresponds to next level set of exponential function
    arr[1]=1.0; // +1.0 is a code for: this is a codomain level set value
    T.push(arr);
    // label(scale(0.7)*format("$%.2f$", r),labelLocationOnFermatsSpiral(log(r),false),deepgreen);
    r=r+0.5;
  };

  U=sort(T);
  write(T.length); // 75 rows
  write(T);
  write(U.length); // 75 rows
  write(U);

  /*---
  for (int i=0; i<U.length; ++i) {
    // first index: row, second index: column
    write(U[i][0]);
  }
  ---*/
     
  for (int i=0; i<U.length; ++i) {
    p=FermatsSpiral(U[i][0]);
    spiral=spiral..p; // extend path 'spiral' by another Bezier node
  }
  return(spiral);
}

// Compute a short straight line through the location on Fermat's spiral corresponding to d
// and perpendicular to the spiral
path bar (pair p, bool domain) {
  real length = 0.15;
  if (domain == true) // we want the tick to point towards the domain value
    return((p.x,p.y) -- (p.x,p.y+(length/2)));
  else
    return((p.x,p.y) -- (p.x,p.y-(length/2)));
}

spiral = FermatsSpiralAxis (spiral);
draw(spiral);

int labelnumber=0; // arrange labels in modulus rows
for (int i=0; i<U.length; ++i) {
  int modulus=2; // 
  if ( U[i][1] > 0.0 ) { // select codomain tickmarks
    // write(U[i][0]);
    pair p = dir( spiral,i,0,true ); // vector showing direction of Fermat's spiral at point i
    // write(p);
    real rotationAngle = degrees( angle(p, false) ); // we want the tickmark to be orthogonal to the curve
    // write(rotationAngle);
    draw( rotate( rotationAngle, point(spiral,i) ) * bar(point(spiral,i), false), deepgreen );
    label( format("$%.2f$", exp(U[i][0])), (point(spiral,i) + rotate(-90) * scale(0.2+0.2*((labelnumber+1)%modulus)) * dir( spiral,i,0,true )), deepgreen );
    // write(0.2*((labelnumber%modulus)+1));
    ++labelnumber;
  }
}

// draw x-Axis as second axis since we want the black ticks to "win" over the green ones

int j=0; // counter to select 1 of incrModulus values
int labelnumber=0;
for (int i=0; i<U.length; ++i) {
  if ( U[i][1] < 0.0 ) { // select domain tickmarks
    // write(U[i][0]);   
    if ((j%incrModulus)==0) { // we limit ourselves to full integer marks
      pair p = dir( spiral,i,0,true ); // vector showing direction of Fermat's spiral at point i
      // write(p);
      real rotationAngle = degrees( angle(p, false) ); // we want the tickmark to be orthogonal to the curve
      // write(rotationAngle);
      draw( rotate( rotationAngle, point(spiral,i) ) * bar(point(spiral,i),true), black );
      int label=(int) U[i][0];
      if (U[i][0]>0.0) {
        ++label;
      }
      label( format("$%d$", label), (point(spiral,i) + rotate(90) * scale(0.2) * dir( spiral,i,0,true )),black);
      // ++labelnumber;
    }
    j=j+1;
  }
}
