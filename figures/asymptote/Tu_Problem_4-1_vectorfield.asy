// Beispiel aus dem Asymptote-Handbuch abgewandelt für 
// .. Beispiel aus Tu, Problem 4.1
import graph;
// Größe der Zeichnung. Einheit "big points", ein big point ist 1/72 inch
// Gibt man 2 Argumente an dann stehen sie für Breite und Höhe
// Mit 100 wird also ein quadratischer Rahmen bereitgestellt, der mehr als 1 inch (2.54 cm) Kantenlänge hat
size(200);
// Zeichne eine box (a,b)
pair a=(-10,-10);
pair b=(10,10);
// Beschriftungen dazu
label("-10,-10",(-11,-11),SW,fontsize(10pt));
label("10,-10",(11,-11),SE,fontsize(10pt));
label("10,10",(11,11),NE,fontsize(10pt));
label("-10,10",(-11,11),NW,fontsize(10pt));
//
xaxis("$x$", xmin=-13,xmax=13,EndArrow);
yaxis("$y$", ymin=-13,ymax=13,EndArrow);
// path ist eine Liste von Punkten, durch "--" verbunden
// Es werden Vektoren z errechnet in Abhängigkeit von (x,y).
// Das Vektorfeld aus Tu, Problem 4.1 ist für alle z identisch
path vector(pair z) {return (0,0)--(z.y,z.x);}
add(vectorfield(vector,a,b));