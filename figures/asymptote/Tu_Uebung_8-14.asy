import graph;
import contour;
// no restriction in x-direction, size defined for y only
size(300,300);
real Ticksize=10;
real x(real t) {return t**2;}
real y(real t) {return t**3;}
draw(graph(x,y,-2,2));
dot((x(-2),y(-2)));
dot((x(-1),y(-1)));
dot((x(0),y(0)));
draw((4,-8)--(0,4),Arrow);
draw((1,-1)--(-1,2),Arrow);
draw((0,0)--(0,0),Arrow);
// contour for concentric circle-function
// see Asymptote manual, for version 2.42-1
real f(real x, real y) {return (x^2+y^2)^(1/2);}
int n=20;
real[] c=new real[n];
for(int i=0; i<=n; ++i) c[i]=(i-n/2);

pen[] p=sequence(new pen(int i) {
    return (c[i] >= 0 ? solid : dashed)+fontsize(6pt);
    },c.length);
    
Label[] Labels=sequence(new Label(int i) {
    return Label(c[i] != 0 ? (string) c[i] : "",Relative(unitrand()),(0,0),
        UnFill(1bp));
    },c.length);
    
draw(Labels,contour(f,(-8,-8),(8,8),c),p);

//limits((-2,-2),(2,4),Crop);
//xaxis("$x$",xmin=-1,xmax=2,BottomTop,LeftTicks(extend=true,step=Ticksize));
//yaxis("$y$",ymin=-1,ymax=3,LeftRight,RightTicks(extend=true,step=Ticksize,trailingzero));
xaxis("$x$",xmin=-8,xmax=8,LeftTicks(extend=true,step=Ticksize));
yaxis("$y$",ymin=-8,ymax=8,RightTicks(extend=true,step=Ticksize,trailingzero));
