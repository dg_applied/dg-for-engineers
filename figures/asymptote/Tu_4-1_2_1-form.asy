// Tu Problem 4.1
// 2018-06-23 pv die zwei Niveau-Flächen (Quadrate) nun so, dass die Normale durch die Mittelpunkte geht.

import graph3;

size(400,0);

currentprojection=perspective(-26,-50,5.5);

real ip; // value of inner product

xaxis3(Label("$x$"),-15,15,axis=YZZero(extend=false),grey,Arrow3);
yaxis3(Label("$y$"),-15,15,axis=XZZero(extend=false),grey,Arrow3);
zaxis3(Label("$z$"),-5,15,axis=XYZero(extend=false),grey,Arrow3);

// für z=1: Rahmen und zugehöriges Fadenkreuz durch den Ursprung
draw((-10,0,1)--(10,0,1),grey);
draw((0,-10,1)--(0,10,1),grey);
// für z=10: Rahmen und zugehöriges Fadenkreuz durch den Ursprung
draw((-10,0,10)--(10,0,10),grey);
draw((0,-10,10)--(0,10,10),grey);

//----- (*) 1-form

// Das war die Idee, statt eines Quadrats eine Tangentialkreisfläche zu zeichnen
// draw(surface(unitcircle3),lightgray,nolight);

// tbd: in dieser Funktion ist der z-Wert
// - einerseits explizit übergeben
// - andererseits in den Rechnungen im Funktionskörper noch vorhanden (deshalb der Name draw1form_1 für den z-Wert 1 und unten draw1form_10 für den z-Wert 10)
// - das ist alles noch sehr weit von einem allgemeinen Programm entfernt
void draw1form_1 (real x, real y, real z) {
  // draw a unit square around x,y,z
  path3 zero_z1=((x-1/sqrt(2),y-1,z-1/sqrt(2))--(x-1/sqrt(2),y+1,z-1/sqrt(2))--(x+1/sqrt(2),y+1,z+1/sqrt(2))--(x+1/sqrt(2),y-1,z+1/sqrt(2))--cycle);
  path3 one_z1=((x+1/2-1/sqrt(2),y-1,z-1/2-1/sqrt(2))--(x+1/2-1/sqrt(2),y+1,z-1/2-1/sqrt(2))--(x+1/2+1/sqrt(2),y+1,z-1/2+1/sqrt(2))--(x+1/2+1/sqrt(2),y-1,z-1/2+1/sqrt(2))--cycle);
  draw(zero_z1,blue);
  draw(one_z1,green);
}

void draw1form_10 (real x, real y, real z) {
  path3 zero_z10=((x-sqrt(1/101),y-1,z-10*sqrt(1/101))--(x-sqrt(1/101),y+1,z-10*sqrt(1/101))--(x+sqrt(1/101),y+1,z+10*sqrt(1/101))--(x+sqrt(1/101),y-1,z+10*sqrt(1/101))--cycle);
  path3 one_z10=((x+10/101-sqrt(1/101),y-1,z-1/101-10*sqrt(1/101))--(x+10/101-sqrt(1/101),y+1,z-1/101-10*sqrt(1/101))--(x+10/101+sqrt(1/101),y+1,z-1/101+10*sqrt(1/101))--(x+10/101+sqrt(1/101),y-1,z-1/101+10*sqrt(1/101))--cycle);
  draw(zero_z10,blue);
  draw(one_z10,green);
}

draw1form_1(-10.0, -10.0, 1.0);
draw1form_1(-10.0, 0.0, 1.0);
draw1form_1(-10.0, 10.0, 1.0);
draw1form_1(0.0, -10.0, 1.0);
draw1form_1(0.0, 0.0, 1.0);
draw1form_1(0.0, 10.0, 1.0);
draw1form_1(10.0, -10.0, 1.0);
draw1form_1(10.0, 0.0, 1.0);
draw1form_1(10.0, 10.0, 1.0);

draw1form_10(-10.0, -10.0, 10.0);
draw1form_10(-10.0, 0.0, 10.0);
draw1form_10(-10.0, 10.0, 10.0);
draw1form_10(0.0, -10.0, 10.0);
draw1form_10(0.0, 0.0, 10.0);
draw1form_10(0.0, 10.0, 10.0);
draw1form_10(10.0, -10.0, 10.0);
draw1form_10(10.0, 0.0, 10.0);
draw1form_10(10.0, 10.0, 10.0);




