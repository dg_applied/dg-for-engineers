import three;
import solids;
size(8cm,0);

// we mimic //calculus123.com/wiki/Cell_complex by Peter Saveliev

currentprojection=perspective(5,3,1.5);
currentlight=light(white,(2,2,2),(2,-2,-2));

// reduction scale for curves which are shown as open intervals
real s=0.90;
// location of 1-cell b (joining A and B)  in degrees
real angle_b=pi/4;
triple A=(sin(angle_b), sin(angle_b), 1);
triple B=(sin(angle_b), sin(angle_b), -1);
real opacity=0.4;

// axes

draw(-2X--2X, grey, Arrow3, L=Label("$x$", position=EndPoint));
draw(-2Y--2Y, grey, Arrow3, L=Label("$y$", position=EndPoint));
draw(-2Z--2Z, grey, Arrow3, L=Label("$z$", position=EndPoint));

// 0-cells

dot(A);
label("$A$",A,N);
dot(B);
label("$B$",B,S);

// 1-cells

path3 a=arc((0,0,1),1, 90,45+3, 90,45-3, CW);
draw(a,MidArcArrow3); 
label("$a$",(-sin(angle_b),-sin(angle_b),1),S);

path3 b=scale(1,1,s)*(A--B);
draw(b,MidArcArrow3);
label("$b$",(sin(angle_b),sin(angle_b),0),E);

path3 c=arc((0,0,-1),1, 90,45-3, 90,45+3, CW);
draw(c,MidArcArrow3);
label("$c$",(-sin(angle_b),-sin(angle_b),-1),N);

// 2-cells

// top disk
draw(shift(0,0,1)*scale(s,s,s)*surface(unitcircle3),lightblue+opacity(opacity));
label("$S_1$",(0,0,1),N);

// bottom disk
draw(shift(0,0,-1)*scale(s,s,s)*surface(unitcircle3),lightblue+opacity(opacity));
label("$S_2$",(0,0,-1),S);

// lateral surface
path3 line=scale(1,1,s)*(A--B);
// revolution about Z-axis
draw(surface(revolution(line,Z,5,355)),lightblue+opacity(opacity));
label("$S_3$",(0,1,0),E);



