import graph;
size(150,0);
real xmin=-4;
real xmax=2;
real startDegrees, endDegrees, xDegrees;
real f(real x) {return exp(x);}
pair F(real x) {return (x,f(x));}
// pair Finv(real x) {return (log(x),0);}

path barOnUnitCircle (real x) {
  if (x < 0)
    xDegrees=degrees(-x);
  else
    xDegrees=-degrees(x);

  return((cos(radians(90+xDegrees))*1.05,sin(radians(90+xDegrees))*1.05) -- (cos(radians(90+xDegrees))*0.95,sin(radians(90+xDegrees))*0.95));
}

pair labelLocationOnUnitCircle (real x, bool inner) {
  if (x < 0)
    xDegrees=degrees(-x);
  else
    xDegrees=-degrees(x);

  if (inner==false)
    return (1.2*(cos(radians(90+xDegrees)),sin(radians(90+xDegrees))));
  else
    return (0.8*(cos(radians(90+xDegrees)),sin(radians(90+xDegrees))));
}

path circularAxis (real start, real end) {
  // Ein Ausschnitt der x-Achse soll auf den Einheitskreis abgebildet.
  // Dabei soll der Nullpunkt dem Nordpol entsprechen.
  if((end - start) > 2*pi) {
    write("passt nicht auf den Einheitskreis");
    return((0,0) -- (0,0));
  }
  else
    if (start < 0)
      startDegrees=degrees(-start);
    else
      startDegrees=-degrees(start);
    if (end < 0)
      endDegrees=degrees(-end);
    else
      endDegrees=-degrees(end);

    return(arc((0,0), 1, 90+startDegrees, 90+endDegrees, CW));
}

// Um die Grafik an die Nachbargrafiken anzugleichen wird der Einheitskreis mit
// Zentrum = (0,1) gezeichnet

draw(shift((0,1))*circularAxis (xmin, xmax));

string label;

for (int i=(int) xmin; i<=(int) xmax; ++i) {
  draw(shift((0,1))*barOnUnitCircle(i), black);
  // >>> hier geht shift nicht .., deshalb (0,1) addieren
  label(format("$%d$", i),labelLocationOnUnitCircle(i,true)+(0,1),black);
}

real r=0.5;
while (log(r)<=xmax) {
  draw(shift((0,1))*barOnUnitCircle(log(r)), green);
  // >>> hier geht shift nicht .., deshalb (0,1) addieren
  label(scale(0.7)*format("$%.2f$", r),labelLocationOnUnitCircle(log(r),false)+(0,1),green);
  r=r+0.5;
}



// label("$e^x$",(1,0),N);
