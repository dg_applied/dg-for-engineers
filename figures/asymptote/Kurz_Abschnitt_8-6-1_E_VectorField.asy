//
// Show Phi, the potential (scalar field) for the exercise in this section of Lehner&Kurz
//
import graph;
defaultpen(1.0);
// unitsize(1cm); // make user coordinates represent multiples of exactly 1 cm
size(16cm,0); // picture shall be 8cm in width, height shall be natural height for this scaling factor
//
// geometry and physics of the exercise
include "./Kurz_Abschnitt_8-6-1_Setting.asy";
//
// vectorfield (see Hammerlindl, 2020, p. 125
//
path vector(pair v) {
  real factor_1   = -Q/(4*pi*epsilon0);
  real factor_2_z = ( -((v.x+a) / (((v.x+a)^2+v.y^2)^(3/2)))
	              +((v.x-a) / (((v.x-a)^2+v.y^2)^(3/2))) );
  real factor_2_r = ( -  v.y    / (((v.x+a)^2+v.y^2)^(3/2))
                      +  v.y    / (((v.x-a)^2+v.y^2)^(3/2))  );
  real x, y;
  x = factor_1 * factor_2_z;
  y = factor_1 * factor_2_r;
  // scale the vector so that it is reduced to the log of its original length
  // this should diminish big discrepancies in vectorlength
  real vectorlength = sqrt(x^2 + y^2);
  real log_vectorlength = log(vectorlength);
  real scale = log_vectorlength / vectorlength;
  write (scale);
  return ( 0 , 0 )--( x*scale , y*scale );
}
// path vector1(pair v) {return (0,0)--(sin(v.x),cos(v.y));} for testing only

// Pay attention not to include location of Q in the grid of vectorfield
// 1. It is better to begin the vectorfield display far away from Q:
//   then, the range of vector lengths is smaller and one can better recognize differences
// 2. Or you use logarithm on the vector length and then you might display the vectorfield
//   in the whole domain minus the location of the charge.
//   In this case there will be a separate scaling for each call of vectorfield()
// add(vectorfield(vector,(-a,-pillbox_stub),(-pillbox_epsilon,pillbox_rmax),nmesh,nmesh));
add(vectorfield(vector,(-a,-pillbox_stub),(0,pillbox_rmax+meshlength),nmesh,nmesh));
//
// we use a second layer in order to hide everything inside the pillbox
layer();
// this is drawn on top of what has been drawn up to now
// hide the part of the vectorfield which is inside the pillbox
path hidingbox = box((0.02,-pillbox_stub*2), (pillbox_epsilon-0.02,pillbox_rmax-0.02));
// pen transparentpen = new pen(opacity=0.0);
filldraw (hidingbox, white, white+opacity(0.01));
draw (hidingbox);
