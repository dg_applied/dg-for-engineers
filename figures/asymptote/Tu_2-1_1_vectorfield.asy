// Tu Problem 2.1
// 2019-01-02 pv in z-Richtung nur -10,0,-10 als Stichproben

import graph3;

size(400,0);

currentprojection=perspective(-26,-50,5.5);

real ip; // value of inner product
int start = -10;
int stop = 10;
int stepsize = 5;
int axisstart = start*2;
int axisstop = stop*2;

xaxis3(Label("$x$"),axisstart,axisstop,axis=YZZero(extend=false),grey,Arrow3);
yaxis3(Label("$y$"),axisstart,axisstop,axis=XZZero(extend=false),grey,Arrow3);
zaxis3(Label("$z$"),axisstart,axisstop,axis=XYZero(extend=false),grey,Arrow3);

// für z=1: Rahmen und zugehöriges Fadenkreuz durch den Ursprung
//draw((-10,0,1)--(10,0,1),grey);
//draw((0,-10,1)--(0,10,1),grey);
// für z=10: Rahmen und zugehöriges Fadenkreuz durch den Ursprung
//draw((-10,0,10)--(10,0,10),grey);
//draw((0,-10,10)--(0,10,10),grey);

//----- (*) vectorfield


for (int i=start; i<=stop; i=i+stepsize) {
  for (int j=start; j<=stop; j=j+stepsize) {
      for (int k=start; k<=stop; k=k+stepsize*2) {
    real x = i;
    real y = j;
    real z = k;
    real vx = i;
    real vy = j;
    real vz = 0;
    real ip;
    string ipstring;
    draw((x,y,z)--(x+vx,y+vy,z+vz),red,Arrow3);
      }
  }
}






