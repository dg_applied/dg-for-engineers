//
// Show the geometrical and physical setting for the exercise in this section of Lehner&Kurz
// There are tweaks on the relation between mesh and pillbox
//
// axes
real zmax=5;
real rmax=5;
//
// layout of diagram, pillbox, mesh
real lt = 0.035; // assumed thickness of line
real a = zmax*0.85; // (arbitrary, just for the look) distance of point charge from origin
// this doesn't change the physics but helps the display of fields,
//   see (*) in the file including this one
// r-coordinate where to start the boundary of the pillbox and the bottom of the vectorfield-box
real pillbox_stub_r = -0.2; 
real pillbox_halfheight = 1;
real pillbox_radius = pillbox_halfheight*3+pillbox_stub_r; // halfheight is small compared to radius
real meshlength = pillbox_halfheight; // to ensure that we get a vector on the surface of the pillbox
//
// parameters from physics
real Q = 1; // Coulomb = As
real epsilon0 = 8.854*10.0^(-12); // As / Vm
//
// draw diagram, pillbox
draw((-zmax,0) -- (zmax,0), arrow=Arrow); // z-axis
label("$z$", (zmax,0), align=SE);
draw((0,2*pillbox_stub_r) -- (0,rmax), arrow = Arrow); // r-axis
label("$r$", (0,rmax), align=NE);
// Gauss pillbox
path domain1 = (-pillbox_halfheight,pillbox_stub_r) -- (-pillbox_halfheight,pillbox_radius) -- (0,pillbox_radius);
path domain2 = (0,pillbox_radius) -- (pillbox_halfheight,pillbox_radius) -- (pillbox_halfheight,pillbox_stub_r);
draw(domain1 -- domain2, dashed); 
// Point P
real Pdist=(4/5)*a; // (arbitrary, just for the look) distance of P from z-axis and from r-axis
pair P=(-Pdist,Pdist);
fill(circle(P,.03));
label("$P$", P, align=N);
// Charges: Q and -Q
fill(circle((-a,0),.05));
label("$Q$", (-a,0), align=N);
fill(circle((a,0),.05));
label("$-Q$", (a,0), align=N);
//
