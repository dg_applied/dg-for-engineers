//
// Show E, the electrical field (1-form) for the exercise in this section of Lehner and Kurz, 2021
// use /module/OneForm_2D_like_asymptote.asy (which mimicks asymptote's module vectorfield.asy)
//
// V 0.8 2024-07-07 computation see Material, Grunewald, 2011,
//   Section "151.2 Formel für graphische Darstellung von Kovektoren"
// 
//
import graph;
defaultpen(1.0);
// unitsize(1cm); // make user coordinates represent multiples of exactly 1 cm
size(16cm,0); // picture shall be 8cm in width, height shall be natural height for this scaling factor
//
// A. geometry and physics of the exercise
include "./Kurz_Abschnitt_8-6-1_Setting.asy";
//
// B. two functions to display a 1-form on a box
//    - OneForm(..) defines a mesh and calls covector_icon(..)
//    - covector_icon(..) returns a specific icon for a covector as a path.
include "./module/OneForm_2D_like_asymptote.asy";
//
// C. given a point v on the manifold and the physics of this setting,
//    compute a single covector as path (Bézier-curve)
path covector(pair v, int comp) {
  // C.1 This is specific to the physics problem
  real factor_1    = Q/(4*pi*epsilon0);
  real term_2_1_dr = (   v.y    / (((v.x+a)^2+v.y^2)^(3/2)) );
  real term_2_1_dz = ( ((v.x+a) / (((v.x+a)^2+v.y^2)^(3/2))));
  real term_2_2_dr = (   v.y    / (((v.x-a)^2+v.y^2)^(3/2)) );
  real term_2_2_dz = ( ((v.x-a) / (((v.x-a)^2+v.y^2)^(3/2))));
  
  real x, y; // (z,r) is specific to the problem, (x,y) is what everyone is used to 
  x = factor_1 * ( term_2_1_dz - term_2_2_dz);
  y = factor_1 * ( term_2_1_dr - term_2_2_dr);
  //
  // scale the 1-form so that it is reduced to the log of its original length
  // this should diminish big discrepancies in vectorlength
  real oneformlength = sqrt(x^2 + y^2);
  real log_oneformlength = log(oneformlength);
  real scale = log_oneformlength / oneformlength;
  //
  // C.2 This is specific to the visualization of a 1-form in 2D
  //
  // C.2.1 an older version where we draw the covector with covector_icon()
  //write(x,y);
  //write(scale);
  //write("covector: ", v, (x*scale , y*scale));
  // 2024-07-07 bl is not necessary anymore since we want to use "arrowbar bar" of asymptote
  // real bl=0.1; // bar length
  // last parameter = 0 - all of the covector
  // last parameter = 1 - start bar
  // last parameter = 2 - end bar
  // last parameter = 3 - tip
  // return covector_icon(v, (x*scale , y*scale), bl, comp);
  //
  // >>>2024-07-06 so würde ein Vektor aussehen
  // return(x*scale, y*scale);
  // >>>2024-07-06 aber wir wollen einen Kovektor zeigen (s. Grunewald, 2011, S. 142)
  //
  // C.2.2 the current version where we draw the covector with standard functions of asymptote
  real xscaled = x*scale;
  real yscaled = y*scale;

  // See Material, Grunewald, 2011,
  //   Section "151.2 Formel für graphische Darstellung von Kovektoren"
  if ( (xscaled^2+yscaled^2) !=0 && (yscaled*(xscaled^2+yscaled^2)) != 0 && yscaled != 0 ) {
    write("1. covector() graphical coordinates: ",
	  xscaled/(xscaled^2+yscaled^2),
	  -xscaled^2/(yscaled*(xscaled^2+yscaled^2)) + 1/yscaled);
    return( xscaled/(xscaled^2+yscaled^2), -xscaled^2/(yscaled*(xscaled^2+yscaled^2)) + 1/yscaled );
  }
  else {
    if (xscaled != 0 && yscaled == 0) {
       write("2.1 covector() graphical coordinates: ",
	     xscaled/(xscaled^2+yscaled^2), 0);
      return( xscaled/(xscaled^2+yscaled^2), 0 );
    }
    else if (xscaled == 0 && yscaled != 0) {
      write("2.2 covector() graphical coordinates: ",
	    0, -xscaled^2/(yscaled*(xscaled^2+yscaled^2))+1/yscaled );
      return( 0, -xscaled^2/(yscaled*(xscaled^2+yscaled^2))+1/yscaled );
    }
    else
      return(0.5,0.5);
  }
}

path covector_all(pair v) {
  return (covector(v, 0));
	  }

path covector_startbar(pair v) {
  return (covector(v, 1));
	  }
path covector_endbar(pair v) {
  return (covector(v, 2));
	  }
path covector_tip(pair v) {
  return (covector(v, 3));
	  }

// part of the box where not to draw vectors.
bool condition(pair v) {
  if (v.x >=0 && v.y < pillbox_radius) return false;
  else return true;
}

//
int nmeshx=6; // n of points at which to draw a vector
int nmeshy=5; // n of points at which to draw a vector
pair location;
add(OneForm_2D_like_asymptote(covector_all,(-(nmeshx-1)*meshlength,pillbox_stub_r),(0,pillbox_radius+meshlength),nmeshx,nmeshy,truesize=false,cond=condition));
//add(OneForm_2D_like_asymptote(covector_startbar,(-(nmeshx-1)*meshlength,pillbox_stub_r),(0,pillbox_radius+meshlength),nmeshx,nmeshy,truesize=false));
//add(OneForm_2D_like_asymptote(covector_endbar,(-(nmeshx-1)*meshlength,pillbox_stub_r),(0,pillbox_radius+meshlength),nmeshx,nmeshy,truesize=false));
//add(OneForm_2D_like_asymptote(covector_tip,(-(nmeshx-1)*meshlength,pillbox_stub_r),(0,pillbox_radius+meshlength),nmeshx,nmeshy,truesize=false));
