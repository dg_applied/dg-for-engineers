//
// Show E, the electrical field (1-form) for the exercise in this section of Lehner and Kurz, 2021
// 
//
import graph;
defaultpen(1.0);
// unitsize(1cm); // make user coordinates represent multiples of exactly 1 cm
size(16cm,0); // picture shall be 8cm in width, height shall be natural height for this scaling factor
//
// A. geometry and physics of the exercise
include "./Kurz_Abschnitt_8-6-1_Setting.asy";
//
// B. iterate over a 2D-domain to show a oneForm on it: OneForm(..) and
//    use a specific icon for a covector: covector_icon(..).
//    The module OneForm_2D defines a mesh and calls covector_icon(..)
//    for each intersection in the mesh.
include "./module/OneForm_2D.asy";
//
// C. given a point v on the manifold and the physics of this setting,
//    compute a single covector as path (Bézier-curve)
pair covector(pair v) {
  // C.1 This is specific to the physics problem
  real factor_1    = Q/(4*pi*epsilon0);
  real term_2_1_dr = (   v.y    / (((v.x+a)^2+v.y^2)^(3/2)) );
  real term_2_1_dz = ( ((v.x+a) / (((v.x+a)^2+v.y^2)^(3/2))));
  real term_2_2_dr = (   v.y    / (((v.x-a)^2+v.y^2)^(3/2)) );
  real term_2_2_dz = ( ((v.x-a) / (((v.x-a)^2+v.y^2)^(3/2))));
  
  real x, y; // (z,r) is specific to the problem, (x,y) is what everyone is used to 
  x = factor_1 * ( term_2_1_dz - term_2_2_dz);
  y = factor_1 * ( term_2_1_dr - term_2_2_dr);
  //
  // scale the 1-form so that it is reduced to the log of its original length
  // this should diminish big discrepancies in vectorlength
  real oneformlength = sqrt(x^2 + y^2);
  real log_oneformlength = log(oneformlength);
  real scale = log_oneformlength / oneformlength;
  //
  // C.2 This is specific to the visualization of a 1-form in 2D
  //
  write(x,y);
  write(scale);
  write(x*scale , y*scale);
  return ( x*scale , y*scale );
}
//
OneForm_2D(covector,(-4,-4),(0,4),3,3);
//
// Pay attention not to include location of Q in the grid of vectorfield
// 1. It is better to begin the vectorfield display far away from Q:
//   then, the range of vector lengths is smaller and one can better recognize differences
// 2. Or you use logarithm on the vector length and then you might display the vectorfield
//   in the whole domain minus the location of the charge.
//   In this case there will be a separate scaling for each call of vectorfield()
/// add(vectorfield(vector,(-a,-pillbox_stub),(-pillbox_epsilon,pillbox_rmax),nmesh,nmesh));
/// add(oneForm(vector,(-a,-pillbox_stub),(0,pillbox_rmax+meshlength),nmesh,nmesh));
//
// we use a second layer in order to hide everything inside the pillbox
///layer();
// this is drawn on top of what has been drawn up to now
// hide the part of the vectorfield which is inside the pillbox
///path hidingbox = box((0.02,-pillbox_stub*2), (pillbox_epsilon-0.02,pillbox_rmax-0.02));
// pen transparentpen = new pen(opacity=0.0);
///filldraw (hidingbox, white, white+opacity(0.01));
///draw (hidingbox);
