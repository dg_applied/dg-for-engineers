// Tu Problem 4.1

import graph3;

size(100,0);

currentprojection=perspective(-26,-50,5.5);

xaxis3(Label("$x$"),-15,15,axis=YZZero(extend=false),grey,Arrow3);
yaxis3(Label("$y$"),-15,15,axis=XZZero(extend=false),grey,Arrow3);
zaxis3(Label("$z$"),-5,15,axis=XYZero(extend=false),grey,Arrow3);

draw((-10,0,1)--(10,0,1),grey);
draw((0,-10,1)--(0,10,1),grey);

draw((-10,0,10)--(10,0,10),grey);
draw((0,-10,10)--(0,10,10),grey);

// compare examples directory of asymptote: vectorfield3.asy
// see handbook asymptote_2018, p. 124f.

// f - square of absolute value of complex z
// abs(z): absolute value of complex z
real f(pair z) {return abs(z)^2;}

// gradient - 3D-vector depending on a pair z
// z - position in xy-plane 
// gradient - vector corresponding to a point z
path3 gradient(pair z) {
  // a specific vectorfield
  return O--(z.y,z.x,0);
}

pair a=(-10,-10);
pair b=(10,10);

triple F1(pair z) {return (z.x,z.y,1);}
triple F2(pair z) {return (z.x,z.y,10);}

// vectorfield(
// - gradient - direction vector (of vector field)
// - F* location vector for starting point of gradient
// - a lower left corner of rectangle on xy-plane to consider
// - b upper right corner of rectangle on xy-plane to consider
// - 2 N of meshes in x-direction
// - 2 N of meshes in y-direction
// - truesize - true = no influence of picture size on size of arrows
// - red color of arrows
//
add(vectorfield(gradient,F1,a,b,2,2,truesize=true,red));
add(vectorfield(gradient,F2,a,b,2,2,truesize=false,red));

draw((-10,0,1)--(-10,-10,1),Arrow3);


