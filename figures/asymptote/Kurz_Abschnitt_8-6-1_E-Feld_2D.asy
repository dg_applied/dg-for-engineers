//
// Show the E-vectorfield for the exercise in this section of Lehner&Kurz
//
import graph;
defaultpen(1.0);
// unitsize(1cm); // make user coordinates represent multiples of exactly 1 cm
size(16cm,0); // picture shall be 8cm in width, height shall be natural height for this scaling factor
//
real a = 4; // distance of point charge from origin
real pillbox_epsilon = 0.5; // half of height of pillbox
real zmax=5;
real rmax=5;
real pillbox_rmax=4;
real pillbox_stub=0.1;
//
// parameters from the physics
real Q = 1; // Coulomb = As
real epsilon0 = 8.854*10.0^(-12); // As / Vm
//
draw((-zmax,0) -- (zmax,0), arrow=Arrow); // z-axis
label("$z$", (zmax,0), align=E);
draw((0,-.1) -- (0,rmax), arrow = Arrow); // r-axis
label("$r$", (0,rmax), align=N);
// Gauss pillbox
draw((pillbox_epsilon,-pillbox_stub) -- (pillbox_epsilon,pillbox_rmax) -- (-pillbox_epsilon,pillbox_rmax) -- (-pillbox_epsilon,-pillbox_stub), dashed); 
// Point P
real Pdist=(4/5)*a; // distance of P from z-axis and from r-axis
pair P=(-Pdist,Pdist);
fill(circle(P,.03));
label("$P$", P, align=N);
// Charges: Q and -Q
fill(circle((-a,0),.05));
label("$Q$", (-a,0), align=N);
fill(circle((a,0),.05));
label("$-Q$", (a,0), align=N);
//
// vectorfield (see Hammerlindl, 2020, p. 125
//
path vector(pair v) {
  real factor_1   = -Q/(4*pi*epsilon0);
  real factor_2_z = ( -((v.x+a) / (((v.x+a)^2+v.y^2)^(3/2)))
	              +((v.x-a) / (((v.x-a)^2+v.y^2)^(3/2))) );
  real factor_2_r = ( -  v.y    / (((v.x+a)^2+v.y^2)^(3/2))
                      +  v.y    / (((v.x-a)^2+v.y^2)^(3/2))  );
  write(factor_1 * factor_2_z);
  write(factor_1 * factor_2_r);
  return ( 0 , 0 )--(factor_1 * factor_2_z, factor_1 * factor_2_r);
}
// path vector1(pair v) {return (0,0)--(sin(v.x),cos(v.y));} das war zum Testen

// Pay attention not to include location of Q in the grid of vectorfield
// It is better to begin the vectorfield display far away from Q:
//   then, the range of vector lengths is smaller and one can better recognize differences
add(vectorfield(vector,(-0.5*a,-pillbox_stub),(-pillbox_epsilon,pillbox_rmax),3,3));
