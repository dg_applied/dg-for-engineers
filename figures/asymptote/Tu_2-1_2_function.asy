// Tu Problem 2.1
// 2019-01-02 pv von http://www.piprime.fr/files/asymptote/solids/fig0090.asy abgeleitet
// opacity: 0 (fully transparent) .. 1 (opaque)

size(8cm,0);
import solids;
import graph3;

currentprojection=orthographic(10,15,3);

real r=10, h=3; // r=sphere radius;
real ri=5; // ri=smaller inner sphere
triple Op=(0,0,h);

limits((0,0,0),1.1*(r,r,r));
axes3("x","y","z");

real rs=sqrt(r^2-h^2); // section radius
real ch=180*acos(h/r)/pi;

path3 arcD=Arc(O,r,180,0,ch,0,Y,50);

revolution sphereD=revolution(O,arcD,Z);
draw(surface(sphereD), opacity(0.5)+lightblue);
draw(shift(0,0,h)*scale3(rs)*surface(unitcircle3),opacity(0.66));

path3 arcU=Arc(O,ri,180,0,-180,0,Y,50);

revolution sphereU=revolution(O,arcU,Z);
draw(surface(sphereU), opacity(0.1)+lightgrey);

if(!is3D())
  shipout(format="pdf", bbox(Fill(paleyellow)));




