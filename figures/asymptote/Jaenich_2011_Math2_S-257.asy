size(10cm,10cm);
usepackage("amssymb");
real ofs=0.2;

// 0-Form
draw((0,0+ofs)--(0,1-ofs),Arrow);
label("$=$",(0,0.5),E);
label("$\mathcal{F}\left(X\right)$", (0,0));
label("$\Omega^0 X$", (0,1));

// 0-Form -> 1-Form
draw((0+ofs,0)--(1-ofs,0),Arrow);
label("$grad$",(0.5,0),N);
draw((0+ofs,1)--(1-ofs,1),Arrow);
label("$d$",(0.5,1),N);

// 1-Form
draw((1,0+ofs)--(1,1-ofs),Arrow);
label("$\cong$",(1,0.5),E);
label("$\mathcal{V}\left(X\right)$", (1,0));
label("$\Omega^1 X$", (1,1));

// 1-Form -> 2-Form
draw((1+ofs,0)--(2-ofs,0),Arrow);
label("$rot$",(1.5,0),N);
draw((1+ofs,1)--(2-ofs,1),Arrow);
label("$d$",(1.5,1),N);

// 2-Form
draw((2,0+ofs)--(2,1-ofs),Arrow);
label("$\cong$",(2,0.5),E);
label("$\mathcal{V}\left(X\right)$", (2,0));
label("$\Omega^2 X$", (2,1));

// 2-Form -> 3-Form
draw((2+ofs,0)--(3-ofs,0),Arrow);
label("$div$",(2.5,0),N);
draw((2+ofs,1)--(3-ofs,1),Arrow);
label("$d$",(2.5,1),N);

// 3-Form
draw((3,0+ofs)--(3,1-ofs),Arrow);
label("$\cong$",(3,0.5),E);
label("$\mathcal{F}\left(X\right)$", (3,0));
label("$\Omega^3 X$", (3,1));


