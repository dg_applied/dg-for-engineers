// Beispiel aus dem Asymptote-Handbuch abgewandelt für 
// .. Beispiel aus Tu, Exercises 4.9
import graph;
// Größe der Zeichnung. Einheit "big points", ein big point ist 1/72 inch
// Gibt man 2 Argumente an dann stehen sie für Breite und Höhe
// Mit 100 wird also ein quadratischer Rahmen bereitgestellt, der mehr als 1 inch (2.54 cm) Kantenlänge hat
size(500);

// 2020-10-25 noch nicht verwendet.
// Wir wollen in Zukunft die gridsize so groß machen, dass der größte Kovektor in einer Zelle Platz hat.
real gridsize = 1;
real max=2;
int imax=2;
// Zeichne eine box (a=unten links,b=oben rechts)
pair a=(-max,0.01);
pair b=(max,max);

// Länge der kleinen Marken (Tangential-"stummel"). Einheitsgitter unterstellt. Dann sollten die Marken 1/10 lang sein.
real bl=2/10;

// Debug (*)
pair DebugPoint=(1,1);  
// Debug (+)

// Beschriftungen dazu
label("-10,-10",(-11,-11),SW,fontsize(6pt));
label("10,-10",(11,-11),SE,fontsize(6pt));
label("10,10",(11,11),NE,fontsize(6pt));
label("-10,10",(-11,11),NW,fontsize(6pt));
//
xaxis("$u$", xmin=-max*1.3,xmax=max*1.3,EndArrow);
yaxis("$v$", ymin=-max*1.3,ymax=max*1.3,EndArrow);

// path ist eine Liste von Punkten, durch "--" verbunden
// Es werden Differentialformen errechnet in Abhängigkeit von (p.x,p.y).

// Offset x-Koordinate Endpunkt gegenüber x-Koordinate Startpunkt
real omegax(real x, real y) {
  // Kehrwert der Koordinatenfunktion
  // Tu, Exercises 4.9
  //if (y != 0)
  //return ((x^2 + y^2) / (-y));
  //else
  // temporär, Notlösung
  //  return 1;

  // x dy
  return 0;
}
// Offset y-Koordinate Endpunkt gegenüber y-Koordinate Startpunkt
real omegay(real x, real y) {
  // Kehrwert der Koordinatenfunktion
  // Tu, Exercises 4.9
  //if (x != 0)
  //return ((x^2 + y^2) / (x));
  //else
    // temporär, Notlösung
  //return 1;

  // für x dy
  return x;
}
    
// Eine vereinfachte Darstellung: Kovektor als Strecke dargestellt mit dickem punkt am Start und kleinem Punkt am Ende
pair covectorStartpoint(pair p) {
  return (p.x,p.y);
}

pair covectorEndpoint(pair p) {
  return (p.x+omegax(p.x, p.y),p.y+omegay(p.x, p.y));
}

real covectorAngleX(pair p) {
  // Waagrechte Linie im Startpoint p der Form nach rechts ziehen, Form selbst, Winkel zwischen beiden
  if (omegax(p.x, p.y)==0) {
    if (omegay(p.x, p.y)>0)
      // Kovektor weist senkrecht nach oben
      return pi/2;
    else
      // Kovektor weist senkrecht nach unten
      return 3*pi/2;
  }
  else {
    if (omegax(p.x, p.y) > 0 && omegay(p.x, p.y) > 0) {
      // Kovektor weist nach rechts oben
      return atan (omegay(p.x, p.y) / omegax(p.x, p.y));
    }
    else if (omegax(p.x, p.y) < 0 && omegay(p.x, p.y) > 0) {
      // Kovektor weist nach links oben
      return (pi + atan (omegay(p.x, p.y) / omegax(p.x, p.y)));
    }
    else if (omegax(p.x, p.y) < 0 && omegay(p.x, p.y) < 0) {
      // Kovektor weist nach links unten
      return (pi + atan (omegay(p.x, p.y) / omegax(p.x, p.y)));
    }
    else if (omegax(p.x, p.y) > 0 && omegay(p.x, p.y) < 0) {
      // Kovektor weist nach rechts unten
      return (2*pi + atan (omegay(p.x, p.y) / omegax(p.x, p.y)));
    }
    else
      return 0;
  }
}

pair covectorHalfBarLengthFromEndpoint(pair p) {
  real covectorTangens;
  real Dx;
  real Dy, DyAbsolute;
  if (omegax(p.x, p.y) != 0) {
    if (omegay(p.x, p.y) != 0) {
      covectorTangens=omegay(p.x, p.y)/omegax(p.x, p.y);
      
      DyAbsolute=sqrt((covectorTangens^2 * (bl/2)^2) / (1 + covectorTangens^2));
      if (0 < covectorAngleX(p) && covectorAngleX(p) < pi/2) {
	Dy=-DyAbsolute;
	Dx=Dy/covectorTangens;

      } else if (pi/2 < covectorAngleX(p) && covectorAngleX(p) < pi) {
	Dy=-DyAbsolute;
	Dx=Dy/covectorTangens;
      } else if (pi < covectorAngleX(p) && covectorAngleX(p) < 3*pi/2) {
	Dy=DyAbsolute;
	Dx=Dy/covectorTangens;
      } else if (3*pi/2 < covectorAngleX(p) && covectorAngleX(p) < 2*pi) {
	Dy=DyAbsolute;
        Dx=DyAbsolute/covectorTangens;
      }
      // Debug (*)
      if (p == DebugPoint) {
      write(p, covectorEndpoint(p), covectorAngleX(p), Dx, Dy, DyAbsolute);
      }
      // Debug (+)
      return (covectorEndpoint(p).x+Dx,covectorEndpoint(p).y+Dy);
    } else { // waagrechter Kovektor
      if (covectorEndpoint(p).x < p.x)
	// Kovektor zeigt nach links
	return (covectorEndpoint(p).x+bl/2,covectorEndpoint(p).y);
      else
        return (covectorEndpoint(p).x-bl/2,covectorEndpoint(p).y);
    }
  } else { // senrechter Kovektor
    if (covectorEndpoint(p).y < p.y)
      // Kovektor zeigt nach unten
      return (covectorEndpoint(p).x,covectorEndpoint(p).y+bl/2);
    else
      return (covectorEndpoint(p).x,covectorEndpoint(p).y-bl/2);
  }
}

path arrowLeftpart(pair p) {
  real phi=covectorAngleX(p);
  return (
	  (
	   (covectorEndpoint(p).x-(bl/2)*sin(phi) + covectorHalfBarLengthFromEndpoint(p).x) / 2,
	   (covectorEndpoint(p).y+(bl/2)*cos(phi) + covectorHalfBarLengthFromEndpoint(p).y) / 2
	  )--( covectorEndpoint(p) )
	 );
}

path arrowRightpart(pair p) {
  real phi=covectorAngleX(p);
  return (
	  (
	   (covectorEndpoint(p).x+(bl/2)*sin(phi) + covectorHalfBarLengthFromEndpoint(p).x) / 2,
	   (covectorEndpoint(p).y-(bl/2)*cos(phi) + covectorHalfBarLengthFromEndpoint(p).y) / 2
	  )--( covectorEndpoint(p) )
	 );
}

path covector(pair p) {
  return covectorStartpoint(p)--covectorEndpoint(p);
}

real covectorLength(pair p) {
  // Abstand zwischen Start- und Endpunkt der Form
  return sqrt(omegax(p.x, p.y)^2+omegay(p.x, p.y)^2);
}

// Kovektor als Paar von kleinen Strecken, genannt Marken, englisch: "bar", dargestellt, die eine Funktionsänderung um 1 darstellen.
path covectorStartbar(pair p) {
  // Winkel der Form mit der x-Achse
  real phi=covectorAngleX(p);
  // Anfangs- und Endpunkt der Marke verbinden
  return ( (p.x-(bl/2)*sin(phi),p.y+(bl/2)*cos(phi))--(p.x+(bl/2)*sin(phi),p.y-(bl/2)*cos(phi)) );
}

path covectorEndbar(pair p) {
  // Winkel der Form mit der x-Achse
  real phi=covectorAngleX(p);
  // Anfangs- und Endpunkt der Marke verbinden
  return ( (covectorEndpoint(p).x-(bl/2)*sin(phi),covectorEndpoint(p).y+(bl/2)*cos(phi))--(covectorEndpoint(p).x+(bl/2)*sin(phi),covectorEndpoint(p).y-(bl/2)*cos(phi)) );
}
  
// Differentialform ähnlich wie Vektorfeld:
// auf einem Gitter aus mit Einheitsabständen wird ausgewertet
// Differentialform wird mit zwei Marken gekennzeichnet
for(int ix=-imax; ix<=imax; ix=ix+1) {
  for (int iy=-imax; iy<=imax; iy=iy+1) {
    if (ix != 0 || iy != 0) {
      pair point=(ix,iy);
      draw(covectorStartbar(point));
      draw(covectorEndbar(point));
     
      // Zu Testzwecken
      //draw(covectorEndpoint(point),red);
      //draw(covectorHalfBarLengthFromEndpoint(point),green);
      // draw(covectorArrow(point));
      
      draw(arrowLeftpart(point));
      draw(arrowRightpart(point));

      // Debug (*)
      if (point == DebugPoint) {
        write(point, covectorHalfBarLengthFromEndpoint(point));
      }
      // Debug (+)
    }
  } 
}
