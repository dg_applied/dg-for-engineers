// 2024-10-28 abgeleitet aus Jaenich_2011_Math2_S-257.asy
// zur Darstellung von Zirnbauer, 2015 "Ungerade Formen", S. 72
// 2024-11-25 die oberen und unteren beiden Zeilen näher aneinandergerückt

size(15cm,15cm);
usepackage("amssymb");
real ofs=0.2;
real ganzoben=2.2;
real oben=1.7;
real unten=-0.7;
real ganzunten=-1.2;

label("$even$", (-0.6,ganzoben));
label("$odd$", (-0.6,oben));

// 0-Form
label("${\Phi}_e$", (0,ganzoben));
label("${\Phi}_m$", (0,oben));

draw((0,0+ofs)--(0,1-ofs),Arrow);
label("$=$",(0,0.5),E);
label("$\mathcal{F}\left(X\right)$", (0,0));
label("$\Omega^0 X$", (0,1));

label("$pseudoscalar$", (0,ganzunten-ofs));
label("${\Phi}_m$", (0,ganzunten));
label("$scalar$", (0,unten-ofs));
label("${\Phi}_e$", (0,unten));

// 0-Form -> 1-Form
draw((0+ofs,ganzoben)--(1-ofs,ganzoben),Arrow);
label("$d$",(0.5,ganzoben),N);
draw((0+ofs,oben)--(1-ofs,oben),Arrow);
label("$d$",(0.5,oben),N);

draw((0+ofs,0)--(1-ofs,0),Arrow);
label("$grad$",(0.5,0),N);
draw((0+ofs,1)--(1-ofs,1),Arrow);
label("$d$",(0.5,1),N);

draw((0+ofs,unten)--(1-ofs,unten),Arrow);
label("$grad$",(0.5,unten),N);
draw((0+ofs,ganzunten)--(1-ofs,ganzunten),Arrow);
label("$grad$",(0.5,ganzunten),N);

// 1-Form
label("$\mathcal{E}$", (1,ganzoben));
label("$\mathcal{H}$", (1,oben));

draw((1,0+ofs)--(1,1-ofs),Arrow);
label("$\cong$",(1,0.5),E);
label("$\mathcal{V}\left(X\right)$", (1,0));
label("$\Omega^1 X$", (1,1));

label("$polar$", (1,unten-ofs));
label("${\overrightarrow{E}}$", (1,unten));
label("$axial$", (1,ganzunten-ofs));
label("${\overrightarrow{H}}$", (1,ganzunten));

// 1-Form -> 2-Form
draw((1+ofs,ganzoben)--(2-ofs,ganzoben),Arrow);
label("$d$",(1.5,ganzoben),N);
draw((1+ofs,oben)--(2-ofs,oben),Arrow);
label("$d$",(1.5,oben),N);

draw((1+ofs,0)--(2-ofs,0),Arrow);
label("$rot$",(1.5,0),N);
draw((1+ofs,1)--(2-ofs,1),Arrow);
label("$d$",(1.5,1),N);

draw((1+ofs,unten)--(2-ofs,unten),Arrow);
label("$rot$",(1.5,unten),N);
draw((1+ofs,ganzunten)--(2-ofs,ganzunten),Arrow);
label("$rot$",(1.5,ganzunten),N);

// 2-Form
label("$\mathcal{B}$", (2,ganzoben));
label("$\mathcal{D}$", (2,oben));

draw((2,0+ofs)--(2,1-ofs),Arrow);
label("$\cong$",(2,0.5),E);
label("$\mathcal{V}\left(X\right)$", (2,0));
label("$\Omega^2 X$", (2,1));

label("${\overrightarrow{B}}$", (2,unten));
label("$axial$", (2,unten-ofs));
label("${\overrightarrow{D}}$", (2,ganzunten));
label("$polar$", (2,ganzunten-ofs));

// 2-Form -> 3-Form
draw((2+ofs,ganzoben)--(3-ofs,ganzoben),Arrow);
label("$d$",(2.5,ganzoben),N);
draw((2+ofs,oben)--(3-ofs,oben),Arrow);
label("$d$",(2.5,oben),N);

draw((2+ofs,0)--(3-ofs,0),Arrow);
label("$div$",(2.5,0),N);
draw((2+ofs,1)--(3-ofs,1),Arrow);
label("$d$",(2.5,1),N);

draw((2+ofs,unten)--(3-ofs,unten),Arrow);
label("$div$",(2.5,unten),N);
draw((2+ofs,ganzunten)--(3-ofs,ganzunten),Arrow);
label("$div$",(2.5,ganzunten),N);

// 3-Form
label("$\mathcal{Q}_m$", (3,ganzoben));
label("$\mathcal{Q}_e$", (3,oben));

draw((3,0+ofs)--(3,1-ofs),Arrow);
label("$\cong$",(3,0.5),E);
label("$\mathcal{F}\left(X\right)$", (3,0));
label("$\Omega^3 X$", (3,1));

label("${\rho}_m$", (3,unten));
label("$pseudoscalar$", (3,unten-ofs));
label("${\rho}_e$", (3,ganzunten));
label("$scalar$", (3,ganzunten-ofs));


