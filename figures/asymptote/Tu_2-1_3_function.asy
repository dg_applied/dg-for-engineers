// Tu Problem 2.1
// 2019-01-02 pv von http://math.mad.free.fr/depot/asysvn/lesfig7.html abgeleitet

import solids;
settings.render=0;
settings.prc=false;

size(200);

revolution r=sphere(O,1);
draw(r,1,longitudinalpen=nullpen);
draw(r.silhouette());

revolution r2=sphere(O,2);
draw(r2,1,longitudinalpen=nullpen);
draw(r2.silhouette());



