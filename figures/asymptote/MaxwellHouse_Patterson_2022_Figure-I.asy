import three;
unitsize(1cm);
size(400);
// alt 2024-12-10 currentprojection=perspective(4,-5,3.5);
currentprojection=perspective(3.6,-5,3.5);
real ofs=0.1; // let enough place for LaTeX-symbol at vertex
real downlabelofs=0.4; // label on downward errors a bit lower than on upward errors
real uplabelofs=0.6; // label on upward errors
real integrationdomain_left=0.8; // dimension of integration domain, left side symbol
real integrationdomain_right=0.7; // dimension of integration domain, right side symbol

// Maxwell house according to Evan Patterson, 2022
// House is represented as different floors, from 0th to 3rd
// Each floor is represented as a square, with
// - front left = 0,0
// - front right = 1,0
// - back left = 0,1
// - back right = 1,1

// 0th floor

int floor=0;

draw((0,0,floor)--(1,0,floor),dashed);
draw((1,0,floor)--(1,1,floor),dashed);
draw((1,1,floor)--(0+ofs,1,floor),dashed); // to 0
draw((0,1-ofs,floor)--(0,0,floor),dashed); // from 0

label("$\mathcal{Q}_{m,V}=0$",(0,1,floor));

triple point_left = (0-integrationdomain_left,0.5,floor);
label("$\int$",point_left);
draw(shift(point_left+(-0.06,0,-0.15))*scale3(0.06)*unitbox);

// defaultpen(0.5); // unit is bp, this is the default line width
triple point_right = (1+integrationdomain_right,0.5,floor);
label("$\int$",point_right);
// defaultpen(0.6); // unit is bp
path3 unitpoint=point_right;
// draw(shift(0,0,-2)*unitpoint);
dot(shift(0,0,-0.1)*unitpoint);


// 1st floor

floor=1;

draw((0+ofs,0,floor)--(1,0,floor),dashed);
draw((1,0,floor)--(1,1-ofs,floor),dashed);
draw((1-ofs,1,floor)--(0+ofs,1,floor),solid,Arrow3); // from H to B
draw((0,1-ofs,floor)--(0,0+ofs,floor),solid,Arrow3); // from B to 0

label("$0$",(0,0,floor));
label("$\mathcal{H}$",(1,1,floor));
label("$\mathcal{B}$",(0,1,floor));

label("$\mu\star$",(0.5,1+ofs,floor));
label("$\partial_t$",(0-ofs,0.5,floor));

triple point_left = (0-integrationdomain_left,0.5,floor);
label("$\int$",point_left);
path3 unitsquare=(point_left+(-0.5,0,0.5)--point_left+(0.5,0,0.5)--point_left+(0.5,0,-0.5)--point_left+(-0.5,0,-0.5)--cycle);
draw(shift(point_left+(0,0,-0.25))*scale3(0.06)*unitsquare);

// defaultpen(0.5); // unit is bp, this is the default line width
triple point_right = (1+integrationdomain_right,0.5,floor);
label("$\int$",point_right);
// defaultpen(0.6); // unit is bp
path3 unitline=(point_right+(-0.05,0,0)--point_right+(0.05,0,0));
draw(shift(0,0,-0.1)*unitline);

// connect 0th floor to 1st floor

draw((0,0,0)--(0,0,1-ofs),dashed); // to 0
draw((1,0,0)--(1,0,1),dashed);
draw((1,1,0)--(1,1,1-ofs),dashed); // to H
draw((0,1,0+ofs)--(0,1,1-ofs),solid,BeginArrow3); // from 0 to B

label("$d$",(0-ofs,1,0+downlabelofs));

// 2nd floor

floor=2;

draw((0+ofs,0,floor)--(1-ofs,0,floor),solid,Arrow3); // from E to D
draw((1,0+ofs,floor)--(1,1-ofs,floor),solid,Arrow3); // from D to J
draw((1-ofs,1,floor)--(0+ofs,1,floor),dashed); // from J to A
draw((0,1-ofs,floor)--(0,0+ofs,floor),solid,Arrow3); // from A to E
draw((0+ofs,0+ofs,floor)--(1-ofs,1-ofs,floor),solid,Arrow3); // diagonally from E to J

label("$\mathcal{E}$",(0,0,floor));
label("$\mathcal{D}$",(1,0,floor));
label("$\mathcal{J}$",(1,1,floor));
label("$\mathcal{A}$",(0,1,floor));

label("$\varepsilon\star$",(0.5,0-ofs,floor));
label("$\sigma\star$",(0.5,0.5-2*ofs,floor));
label("$-\partial_t$",(1+ofs,0.5,floor));
label("$-\partial_t$",(0-ofs,0.5,floor));

triple point_left = (0-integrationdomain_left,0.5,floor);
label("$\int$",point_left);
path3 unitline=(point_left+(-0.05,0,0)--point_left+(0.05,0,0));
draw(shift(0,0,-0.1)*unitline);

// defaultpen(0.5); // unit is bp, this is the default line width
triple point_right = (1+integrationdomain_right,0.5,floor);
label("$\int$",point_right);
path3 unitsquare=(point_left+(-0.5,0,0.5)--point_left+(0.5,0,0.5)--point_left+(0.5,0,-0.5)--point_left+(-0.5,0,-0.5)--cycle);
draw(shift(point_right+(0.03,0,-0.25))*scale3(0.06)*unitsquare);

// connect 1st floor to 2nd floor

draw((0,0,1+ofs)--(0,0,2-ofs),solid,BeginArrow3); // from 0 to E
draw((1,0,1)--(1,0,2-ofs),dashed); // to D
draw((1,1,1+ofs)--(1,1,2-ofs),solid,Arrow3); // from H to J
draw((0,1,1+ofs)--(0,1,2-ofs),solid,BeginArrow3); // from B to A

label("$-d$",(0-ofs,0,1+downlabelofs)); // from 0 to E
label("$d$",(1+ofs,1,1+uplabelofs)); // from H to J
label("$d$",(0-ofs,1,1+downlabelofs)); // from B to A

// 3rd floor

floor=3;

draw((0+ofs,0,floor)--(1-ofs,0,floor),dashed); // from Phi to rho
draw((1,0+ofs,floor)--(1,1,floor),dashed); // from rho
draw((1,1,floor)--(0,1,floor),dashed); 
draw((0,1,floor)--(0,0+ofs,floor),dashed); // to rho

label("$\Phi$",(0,0,floor));
label("$\mathcal{Q}_{e,V}$",(1,0,floor));

label("$\int$",(0-integrationdomain_left,0.5,floor));
label("$\int$",(1+integrationdomain_right,0.5,floor));

triple point_left = (0-integrationdomain_left,0.5,floor);
label("$\int$",point_left);
// defaultpen(0.6); // unit is bp
path3 unitpoint=point_left;
// draw(shift(0,0,-2)*unitpoint);
dot(shift(0,0,-0.1)*unitpoint);

// defaultpen(0.5); // unit is bp, this is the default line width
triple point_right = (1+integrationdomain_right,0.5,floor);
label("$\int$",point_right);
draw(shift(point_right+(-0.06,0,-0.15))*scale3(0.06)*unitbox);

// connect 2nd floor to 3rd floor

draw((0,0,2+ofs)--(0,0,3-ofs),solid,BeginArrow3); // from E to Phi
draw((1,0,2+ofs)--(1,0,3-ofs),solid,Arrow3); // from D to rho
draw((1,1,2+ofs)--(1,1,3),dashed); // from J
draw((0,1,2+ofs)--(0,1,3),dashed); // from A

label("$-d$",(0-ofs,0,2+downlabelofs)); // from E to Phi
label("$d$",(1+ofs,0,2+uplabelofs)); // from D to rho
