
import graph;
size(7cm);
pair a=(-2,-2);
pair b=(2,2);
path vector(pair z) {return (0,0)--(1,z.y-z.x);}
add(vectorfield(vector,a,b));

real c=1.0;
real f1(real x) {
  return (c*exp(x)+x+1);
}
real c=0.0;
real f2(real x) {
  return (c*exp(x)+x+1);
}
real c=-1.0;
real f3(real x) {
  return (c*exp(x)+x+1);
}
path g1 = graph(f1, a.x,0);
path g2 = graph(f2, a.x,1);
path g3 = graph(f3, a.x,1.5);
draw(g1, arrow=Arrow(TeXHead), red);
draw(g2, arrow=Arrow(TeXHead), black);
draw(g3, arrow=Arrow(TeXHead), blue);

xaxis("$x$");
yaxis("$y$");
