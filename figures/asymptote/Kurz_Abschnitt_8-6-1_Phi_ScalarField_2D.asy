//
// Show Phi, the potential (scalar field) for the exercise in this section of Lehner&Kurz
//
import graph;
import contour;
defaultpen(1.0);
// unitsize(1cm); // make user coordinates represent multiples of exactly 1 cm
size(16cm,0); // picture shall be 8cm in width, height shall be natural height for this scaling factor
//
// geometry and physics of the exercise
//
include "./Kurz_Abschnitt_8-6-1_Setting.asy";
//
// scalar field with contour plot (see Hammerlindl, 2020, p. 156
//
real Phi(real z, real r) {
  // this implements Lehner, 2021, p. 602
  real factor_1   = Q/(4*pi*epsilon0);
  real Rplus      = sqrt((r^2)+(z+a)^2);
  real Rminus     = sqrt((r^2)+(z-a)^2);

  if (Rplus == 0 || Rminus == 0) return 0;
  real result     = factor_1 * ( 1/Rplus - 1/Rminus);
  if (result <= 1.0) return 0;
  else {
    // write(log(result));
    return log(result);
    // return (result);
  }
}

// see Hammerlindl, 2020, p. 158
// draw contour line where value of log(Phi) is (0, 1, .., 39)
int n=40;
real [] c=new real[n];
for (int i=0; i < n; ++i) c[i] = i;
// see Hammerlindl, 2020, p. 158
Label[] Labels=sequence(new Label(int i) {
    return Label(c[i] != 0 && c[i]%2==0 && c[i] > 18 && c[i] < 26 ?
		 (string) c[i] : "", Relative(unitrand()),(0,0),UnFill(1bp));
		 },c.length);
int nmeshx=5;
// draw(contour(Phi, (-a,pillbox_stub_r),(0,pillbox_radius), c));
  draw(Labels,contour(Phi, (-nmeshx*meshlength,pillbox_stub_r),(0,pillbox_radius+meshlength),c));
//
// we use a second layer in order to hide vectors in that part of the pillbox which is in domain 2
layer();
// this is drawn on top of what has been drawn up to now
// hide the part of the vectorfield which is above z-axis
path hidingbox = box((lt,lt), (pillbox_halfheight-lt,pillbox_radius-lt));
// first color is for filling, second for boundary
filldraw (hidingbox, white, white);
// hide the part of the vectorfield which is below z-axis
// "-meshlength-3*lt" manual tweaking here
path hidingbox = box((lt,pillbox_stub_r), (pillbox_halfheight-lt,-lt));
// first color is for filling, second for boundary
filldraw (hidingbox, white, white);

